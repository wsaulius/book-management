package lt.sda.academy.data;

import lt.sda.academy.model.Author;
import lt.sda.academy.model.Book;
import lt.sda.academy.presentation.AuthorChoice;
import lt.sda.academy.presentation.BookChoice;
import lt.sda.academy.presentation.BookDataPrinter;
import lt.sda.academy.util.HibernateUtil;
import lt.sda.academy.validators.AuthorValidator;
import org.hibernate.Session;

import javax.persistence.Query;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class BookService {

    private final AuthorValidator authorValidator = new AuthorValidator();

    public void listAllBooks() {

        Session session = HibernateUtil.getNextSession();
        Query query = session.createQuery("from Book");

        List<Book> allBooks = query.getResultList();
        BookDataPrinter.printBookPrimaryInfo(allBooks);
    }

    public void createNewBook() {

        String authorName = AuthorChoice.selectAuthorsName();
        while (!authorValidator.isNameOrSurnameValid(authorName)) {
            AuthorChoice.sayThatFieldWasNotValid("Vardas");
            authorName = AuthorChoice.selectAuthorsName();
        }

        String authorSurname = AuthorChoice.selectAuthorsSurname();
        while (!authorValidator.isNameOrSurnameValid(authorSurname)) {
            AuthorChoice.sayThatFieldWasNotValid("Pavardė");
            authorSurname = AuthorChoice.selectAuthorsName();
        }

        Session newSession = HibernateUtil.getSessionFactory().openSession();

        Author author = new Author();
        author.setName(authorName);
        author.setSurname(authorSurname);

        newSession.save(author);


        Book book = new Book();
        String bookIBSN = BookChoice.selectBookIBSN();
        String bookTitle = BookChoice.selectBookTitle();

        book.setIsbn(bookIBSN);
        book.setTitle(bookTitle);
        book.setPublishedDate(new Date());

        author.setBook(book);
        book.setAuthors(Arrays.asList(author));

        BookDataPrinter.printBookPrimaryInfo(Arrays.asList(book));

        newSession.save(book);
        newSession.close();

        BookDataPrinter.bookSaveSuccessPrint(book);
    }
}
