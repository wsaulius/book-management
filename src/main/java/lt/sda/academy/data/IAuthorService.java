package lt.sda.academy.data;

public interface IAuthorService {

    public void createNewAuthor();
    public void listAllAuthors();

}
