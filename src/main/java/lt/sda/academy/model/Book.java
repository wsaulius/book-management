package lt.sda.academy.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Data
@Table(name = "Books")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String isbn;
    private String title;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "book")
    private List<Author> authors = new ArrayList<>();

    @Temporal(TemporalType.DATE)
    @Column(name = "PUBLISHED_DATE")
    private Date published;

    public Book() {
    }

    public Book(String isbn, String title, Date published) {
        this.isbn = isbn;
        this.title = title;
        this.published = published;
    }

    public String getIsbn() {
        return this.isbn;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthors(final List<Author> authors) {
        this.authors = new ArrayList<>( authors );
    }

    public List<Author> getAuthors() {
        return this.authors;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Date getPublishedDate() {
        return this.published;
    }

    public void setPublishedDate(Date published) {
        this.published = published;
    }

    @Override
    public String toString() {
        final String authorNames = this.authors.stream().map(Author::getName).collect(Collectors.joining(", "));
        return MessageFormat.format("{0} by {1} (ISBN: {2}), published {3}", this.title, authorNames, this.isbn, this.published);
    }
}
