package lt.sda.academy.presentation;

import lt.sda.academy.model.Author;

import java.util.List;

public class AuthorDataPrinter {

    public static void authorSaveSuccessPrint(Author author) {
        System.out.println("Autorius buvo išsaugotas sėkmingai: ");
        printAuthorPrimaryFields(author);
    }

    public static void printAuthorsPrimaryInfo(List<Author> authorList) {
        System.out.println("Visu autorių informacija: ");
        for (Author author : authorList) {
            printAuthorPrimaryFields(author);
        }
    }

    private static void printAuthorPrimaryFields(Author author) {
        System.out.printf("Id: %s, Vardas: %s, Pavardė: %s%n", author.getId(), author.getName(), author.getSurname());
    }
}
