package lt.sda.academy.presentation;

import lt.sda.academy.model.Book;
import java.util.List;

public class BookDataPrinter {

    public static void bookSaveSuccessPrint(Book book) {
        System.out.println("Knyga buvo išsaugota sėkmingai: ");
        printBookPrimaryFields(book);
    }

    public static void printBookPrimaryInfo(List<Book> bookList) {

        System.out.println("Visu knygų informacija: ");
        for (Book book : bookList) {
            printBookPrimaryFields(book);
        }
    }

    private static void printBookPrimaryFields(Book book) {
        System.out.printf("Id: %s, Pavadinimas: %s, Autoriai: %s%n",
                book.getIsbn(), book.getTitle(), book.getAuthors());
    }

}

